/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright (c) 2022 Rockchip Electronics Co., Ltd.
 */

/dts-v1/;
/ {
	description = "FIT source file for rockchip AMP";
	#address-cells = <1>;

	images {
		amp1 {
			description  = "bare-mental-core1";
			data         = /incbin/("hal1.bin");
			type         = "firmware";
			compression  = "none";
			arch         = "arm";
			cpu          = <0x100>;
			thumb        = <0>;
			hyp          = <0>;
			load         = <0x01800000>;
			udelay       = <10000>;
			hash {
				algo = "sha256";
			};
		};

		amp2 {
			description  = "bare-mental-core2";
			data         = /incbin/("hal2.bin");
			type         = "firmware";
			compression  = "none";
			arch         = "arm";
			cpu          = <0x200>;
			thumb        = <0>;
			hyp          = <0>;
			load         = <0x02000000>;
			udelay       = <10000>;
			hash {
				algo = "sha256";
			};
		};

		amp3 {
			description  = "bare-mental-core3";
			data         = /incbin/("hal3.bin");
			type         = "firmware";
			compression  = "none";
			arch         = "arm";
			cpu          = <0x300>;
			thumb        = <0>;
			hyp          = <0>;
			load         = <0x02800000>;
			udelay       = <10000>;
			hash {
				algo = "sha256";
			};
		};
	};

	configurations {
		default = "conf";
		conf {
			description = "Rockchip AMP images";
			rollback-index = <0x0>;
			loadables = "amp1", "amp2", "amp3";

			signature {
				algo = "sha256,rsa2048";
				padding = "pss";
				key-name-hint = "dev";
				sign-images = "loadables";
			};

			/* - run linux on cpu0
			 * - it is brought up by amp(that run on U-Boot)
			 * - it is boot entry depends on U-Boot
			 */
			linux {
				description  = "linux-os";
				arch         = "arm64";
				cpu          = <0x000>;
				thumb        = <0>;
				hyp          = <0>;
				udelay       = <0>;
			};
		};
	};
};
